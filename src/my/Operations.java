package my;

import java.util.*;

public class Operations {

    public void numCardAndSum(ArrayList base){
        int tempNum = 0;
        double sum = 0;
        int prev = 0;
        for (int i = 0; i < base.size(); i++) {
            if(getNumCard(i, base) != prev) {
                tempNum = getNumCard(i, base);
                for (int j = 0; j < base.size(); j++) {
                    if (getNumCard(j, base) == tempNum) {
                        sum += getCost(j, base);
                    }
                }
                System.out.println("Number card: " + getNumCard(i, base) + "\tSumma: " + sum);
            }
            prev = getNumCard(i, base);
            sum = 0;
        }
    }

    public void setPercent(int n, ArrayList base){
        Storage temp = (Storage) base.get(n);
        temp.setPercent(getDiscount(n, base) * 100 / (getCost(n, base) + getDiscount(n, base)));
    }

    public int getNumCard(int n, ArrayList base){
        Storage temp = (Storage) base.get(n);
        return temp.getNumber();
    }

    public double getCost(int n, ArrayList base){
        Storage temp = (Storage) base.get(n);
        return temp.getCost();
    }

    public void setDiscount(int n, ArrayList base, double discount){
        Storage temp = (Storage) base.get(n);
        temp.setDiscount(discount);
    }

    public double getDiscount(int n, ArrayList base){
        Storage temp = (Storage) base.get(n);
        return temp.getDiscount();
    }

    public void setCost(int n, ArrayList base, double cost){
        Storage temp = (Storage) base.get(n);
        temp.setCost(cost);
    }

    public String getName(int n, ArrayList base){
        Storage temp = (Storage) base.get(n);
        return temp.getName();
    }

    public Date getDate(int n, ArrayList base){
        Storage temp = (Storage) base.get(n);
        return temp.getDate();
    }

    public void createDiscount(ArrayList base, double PERCENT5){
        for (int i = 0; i < base.size(); i++) {
            setDiscount(i, base, getCost(i, base) * PERCENT5);
            setCost(i, base, getCost(i, base) - (getCost(i, base) * PERCENT5));
        }
    }

    public void createDiscount(ArrayList base){
        String prev;
        for (int i = 0; i < base.size(); i++) {
            String tempStr = getName(i, base);
            for (int j = i + 1; j < base.size(); j++) {
                prev = getName(j, base);
                double PERCENT0 = 0;
                if (tempStr.equals(prev)) {//check name shops
                    //equals Date and set Discount
                    Date dateOne = getDate(i, base);
                    Date dateTwo = getDate(j, base);
                    // Количество дней между датами в миллисекундах
                    long difference = dateOne.getTime() - dateTwo.getTime();
                    // Перевод количества дней между датами из миллисекунд в дни
                    int days = (int) (difference / (24 * 60 * 60 * 1000)); // миллисекунды / (24ч * 60мин * 60сек * 1000мс)
                    days = Math.abs(days);
                    double WEEK = 7;
                    if (days <= WEEK) {
                        double PERCENT5 = 0.05;
                        setDiscount(j, base, (getCost(j, base) * PERCENT5));
                        setCost(j, base, getCost(j, base) - (getCost(j, base) * PERCENT5));
                        setPercent(j, base);
                    } else {
                        double FOURWEEK = 28;
                        if (days > WEEK && days <= FOURWEEK) {
                            double PERCENT10 = 0.1;
                            setDiscount(j, base, (getCost(j, base) * PERCENT10));
                            setCost(j, base, getCost(j, base) - (getCost(j, base) * PERCENT10));
                            setPercent(j, base);
                        } else {
                            setDiscount(j, base, getCost(j, base) * PERCENT0);
                            setCost(j, base, getCost(j, base) - (getCost(j, base) * PERCENT0));
                            setPercent(j, base);
                        }
                    }
                }
            }
        }
    }
}

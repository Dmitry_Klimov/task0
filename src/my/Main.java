package my;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        Storage base = new Storage();
        Operations operations = new Operations(); //дата: double - String - dateFormat
        try {
            base.reader(args[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        ArrayList data = base.getBase();
        operations.createDiscount(data);
        base.setBase(data);
        base.printerBase();
        base.sortBaseDate();
        base.printerBase();
        base.sortBaseNumCard();
        data = base.getBase();
        operations.numCardAndSum(data);
    }
}

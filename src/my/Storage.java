package my;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Storage{
    private int CONST = 6;
    private Date date;
    private String name;
    private String purchase;
    private int number;
    private double cost;
    private double discount;
    private double percent;


    private ArrayList base = new ArrayList<Storage>();


    public ArrayList reader(String fileName)throws Exception{
        Scanner scan;
        try {
            scan = new Scanner(new FileReader(fileName));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        while(scan.hasNextLine()){
            String str = scan.nextLine();
            String[] n_str = str.split(";");
            if(n_str.length == CONST) {
                Storage obj = new Storage(stringToDate(n_str[0]), n_str[1], n_str[2], Integer.parseInt(n_str[3]), Double.parseDouble(n_str[4]), Double.parseDouble(n_str[5]), 0);
                base.add(obj);
            }else{
                System.out.println("Invalid Data. Exit.");
                System.exit(0);
            }
        }
        return base;
    }

    public void printerBase(){
        Storage user;
        for (int i = 0; i < base.size(); i++) {
            user = (Storage) base.get(i);
            System.out.printf(" %s %td.%<tm.%<tY\n", "Дата: ", user.date);
            System.out.println(
                    " Магазин: " + user.name +
                    "\n Покупка: " + user.purchase +
                    "\n Номер карты: " + user.number +
                    "\n Цена: " + user.cost +
                    "\n Скидка: " + user.discount +
                    "\tСкидка в процентах: " + user.percent + "%");
            System.out.println();
        }
        System.out.println("\n");
    }

    public Date stringToDate(String str) {
        SimpleDateFormat ft = new SimpleDateFormat("dd.MM.yyyy");
        Date parsingDate = null;
        try {
            parsingDate = ft.parse(str);
        } catch (ParseException e) {
            System.out.println("Нераспаршена с помощью " + ft);
        }
        return parsingDate;
    }

    public Comparator<Storage> DateComparator = new Comparator<Storage>() {
        @Override
        public int compare(Storage o1, Storage o2) {
            return o1.getDate().compareTo(o2.getDate());
        }
    };

    public void sortBaseDate(){
        Collections.sort(base, DateComparator);
    }

    public Comparator<Storage> NumCardComparator = new Comparator<Storage>() {
        @Override
        public int compare(Storage o1, Storage o2) {
            return o1.getNumber() - o2.getNumber();
        }
    };

    public void sortBaseNumCard(){
        Collections.sort(base, NumCardComparator);
    }

    public Storage(Date date, String name, String purchase, int number, double cost, double discount, double percent) {
        this.date = date;
        this.name = name;
        this.purchase = purchase;
        this.number = number;
        this.cost = cost;
        this.discount = discount;
        this.percent = percent;
    }

    public Storage() {
    }

    public double getPercent() {
        return percent;
    }

    public void setPercent(double percent) {
        this.percent = percent;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPurchase() {
        return purchase;
    }

    public void setPurchase(String purchase) {
        this.purchase = purchase;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public ArrayList getBase() {
        return base;
    }

    public void setBase(ArrayList base) {
        this.base = base;
    }
}
